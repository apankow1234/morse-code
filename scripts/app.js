function toCode( message ) {
    code = [];
    messageTokens = message.toUpperCase().split('').map( t => {
        code.push( MorseCodeToken.GetByMappingVal( t ) );
    } );
    let codeStr = MorseCodeWordToken.GetVal( code );
    MorseCodeWordToken.Output( code );
    console.log( message );
    console.log( codeStr );
    return codeStr
}

window.onload = () => {
    let btn = document.createElement('button');
    btn.innerText = "Play Message";
    let header = document.createElement('h1');
    header.innerText = "Titanic's Last Words";
    let engl = document.createElement('p');
    engl.innerText = "SOS SOS CQD CQD - MGY WE ARE SINKING FAST PASSENGERS BEING PUT INTO BOATS - MGY";
    document.body.appendChild( btn );
    document.body.appendChild( header );
    document.body.appendChild( engl );
    let code = document.createElement('p');
    document.body.appendChild( code );
    document.querySelector('button').addEventListener('click', function() {
        s.context.resume();
        let codeStr = toCode( engl.innerText );
        code.innerHTML = codeStr.replace( /\s/g, `&nbsp;` );
    });
}