/**
 * These constants are performing similar tasks to C/C++ Preprocessor Function Macros 
 * to create and initiate Classes dynamically
 */
const buildRawAsciiToken = ( name, prototype, char ) => {
	return ({ [name] : class extends (prototype) { static GetVal() { return char; } } })[name];
}
const buildMorseCodePatternToken = ( name, prototype, contents ) => {
	name = 'MorseCode_'+name.replace( /\W/g, '' );
	return ({ [name] : class extends (prototype) { static GetVal() { return (prototype).GetVal( contents ); } } })[name];
}
const initAsciiRawToken = ( hr, mc, hrVal, mcVal ) => {
	/** Values */
	hr.Val      = hrVal;
	mc.Contents = mcVal;
	/** Mappings */
	hr.Mapping = mc;
	mc.Mapping = hr;
	/** Storage */
    MorseCodeToken.allTokens.push( mc );
    HumanReadableToken.allTokens.push( hr );
}
const defineAsciiGlyph = ( type, char, contents ) => {
	name = type.replace(/\W/g,'')+'_'+char;
    let newHumanReadableToken = buildRawAsciiToken(         name, HumanReadableGlyphToken, char     );
    let newMorseCodeToken     = buildMorseCodePatternToken( char, MorseCodePatternToken,   contents );
	initAsciiRawToken( newHumanReadableToken, newMorseCodeToken, char, contents );
}
const defineAsciiDelimiter = ( name, char, contents ) => {
	name = name.replace(/\W/g,'');
    let newHumanReadableToken = buildRawAsciiToken( 'Delim_'+name, HumanReadableDelimiterToken, char     );
    let newMorseCodeToken     = buildMorseCodePatternToken(  name, MorseCodePatternToken,       contents );
	initAsciiRawToken( newHumanReadableToken, newMorseCodeToken, char, contents );
}
const _ = ( c ) => { return MorseCodeToken.Get( c ); }
const di  = dit = MorseCodeDotSignalToken;
const dah = MorseCodeDashSignalToken;
const hmm = MorseCodeBlankSignalToken;


/**
 * Raw Tokens
 */
defineAsciiGlyph(     'Letter',              `A`,  [ di,  dah ]                          );
defineAsciiGlyph(     'Letter',              `B`,  [ dah, di,  di,  dit ]                );
defineAsciiGlyph(     'Letter',              `C`,  [ dah, di,  dah, dit ]                );
defineAsciiGlyph(     'Letter',              `D`,  [ dah, di,  dit ]                     );
defineAsciiGlyph(     'Letter',              `E`,  [ dit ]                               );
defineAsciiGlyph(     'Letter',              `F`,  [ di,  di,  dah, dit ]                );
defineAsciiGlyph(     'Letter',              `G`,  [ dah, dah, dit ]                     );
defineAsciiGlyph(     'Letter',              `H`,  [ di,  di,  di,  dit ]                );
defineAsciiGlyph(     'Letter',              `I`,  [ di,  dit ]                          );
defineAsciiGlyph(     'Letter',              `J`,  [ di,  dah, dah, dah ]                );
defineAsciiGlyph(     'Letter',              `K`,  [ dah, di,  dah ]                     );
defineAsciiGlyph(     'Letter',              `L`,  [ di,  dah, di,  dit ]                );
defineAsciiGlyph(     'Letter',              `M`,  [ dah, dah ]                          );
defineAsciiGlyph(     'Letter',              `N`,  [ dah, dit ]                          );
defineAsciiGlyph(     'Letter',              `O`,  [ dah, dah, dah ]                     );
defineAsciiGlyph(     'Letter',              `P`,  [ di,  dah, dah, dit ]                );
defineAsciiGlyph(     'Letter',              `Q`,  [ dah, dah, di,  dah ]                );
defineAsciiGlyph(     'Letter',              `R`,  [ di,  dah, dit ]                     );
defineAsciiGlyph(     'Letter',              `S`,  [ di,  di,  dit ]                     );
defineAsciiGlyph(     'Letter',              `T`,  [ dah ]                               );
defineAsciiGlyph(     'Letter',              `U`,  [ di,  di,  dah ]                     );
defineAsciiGlyph(     'Letter',              `V`,  [ di,  di,  di,  dah ]                );
defineAsciiGlyph(     'Letter',              `W`,  [ di,  dah, dah ]                     );
defineAsciiGlyph(     'Letter',              `X`,  [ dah, di,  di,  dah ]                );
defineAsciiGlyph(     'Letter',              `Y`,  [ dah, di,  dah, dah ]                );
defineAsciiGlyph(     'Letter',              `Z`,  [ dah, dah, di,  dit ]                );
defineAsciiGlyph(     'Digit',               `0`,  [ dah, dah, dah, dah, dah ]           );
defineAsciiGlyph(     'Digit',               `1`,  [ di,  dah, dah, dah, dah ]           );
defineAsciiGlyph(     'Digit',               `2`,  [ di,  di,  dah, dah, dah ]           );
defineAsciiGlyph(     'Digit',               `3`,  [ di,  di,  di,  dah, dah ]           );
defineAsciiGlyph(     'Digit',               `4`,  [ di,  di,  di,  di,  dah ]           );
defineAsciiGlyph(     'Digit',               `5`,  [ di,  di,  di,  di,  dit ]           );
defineAsciiGlyph(     'Digit',               `6`,  [ dah, di,  di,  di,  dit ]           );
defineAsciiGlyph(     'Digit',               `7`,  [ dah, dah, di,  di,  dit ]           );
defineAsciiGlyph(     'Digit',               `8`,  [ dah, dah, dah, di,  dit ]           );
defineAsciiGlyph(     'Digit',               `9`,  [ dah, dah, dah, dah, dit ]           );
defineAsciiDelimiter( 'Pattern Separator',   ``,   [ hmm, hmm, hmm ]                     );
defineAsciiDelimiter( 'Word Separator',      ` `,  [ hmm, hmm, hmm, hmm, hmm, hmm, hmm ] );
defineAsciiDelimiter( 'Full Stop',           `\.`, [ di,  dah, di,  dah, di,  dah ]      );
defineAsciiDelimiter( 'Comma',               `\,`, [ dah, dah, di,  di,  dah, dah ]      );
defineAsciiDelimiter( 'Colon',               `\:`, [ dah, dah, dah, di,  di,  dit ]      );
defineAsciiDelimiter( 'Question Mark',       `\?`, [ di,  di,  dah, dah, di,  dit ]      );
defineAsciiDelimiter( 'Apostrophe',          `\'`, [ di,  dah, dah, dah, dah, dit ]      );
defineAsciiDelimiter( 'Hyphen',              `\-`, [ di,  dah, dah, dah, dah, dit ]      );
defineAsciiDelimiter( 'Division Sign',       `\/`, [ dah, di,  di,  dah, dit ]           );
defineAsciiDelimiter( 'Parenthesis Left',    `\(`, [ dah, di,  dah, dah, dit ]           );
defineAsciiDelimiter( 'Parenthesis Right',   `\)`, [ dah, di,  dah, dah, di,  dah ]      );
defineAsciiDelimiter( 'Quotation Mark',      `\"`, [ di,  dah, di,  di,  dah, dit ]      );
defineAsciiDelimiter( 'Equals Sign',         `\=`, [ dah, di,  di,  di,  dah ]           );
defineAsciiDelimiter( 'Plus Sign',           `\+`, [ di,  dah, di,  dah, dit ]           );
// defineAsciiDelimiter( 'Multiplication Sign', `\×`, [ dah, di,  di,  dah ]                ); /* Same as 'X' */
defineAsciiDelimiter( 'Commercial At',       `\@`, [ di,  dah, dah, di,  dah, dit ]      );
defineAsciiDelimiter( 'Erase Last',          `\b`, [ di,  di,  di,  di,  di,  di,  di,  dit ] );


/**
 * Compound Tokens
 */
// defineMorseCodeDelimiter( 'Percent Sign',    `\%`, [ '0', '/', '0' ]                     );
// defineMorseCodeDelimiter( 'Per Mille Sign',  `\‰`, [ '0', '/', '0', '0' ]                );


/**
 * Abbreviations
 */
class MorseCodeCQAbbreviation extends MorseCodeAbbreviationToken { 
	constructor(){ super([]); }
	static GetVal(){ return MorseCodeAbbreviationToken.GetVal( MorseCodeCQAbbreviation.Contents ) }
}
MorseCodeCQAbbreviation.Contents = [ _('C'), _('Q') ];

class MorseCodeCQDAbbreviation extends MorseCodeAbbreviationToken { 
	constructor(){ super([]); }
	static GetVal(){ return MorseCodeAbbreviationToken.GetVal( MorseCodeCQDAbbreviation.Contents ) }
}
MorseCodeCQDAbbreviation.Contents = [ _('C'), _('Q'), _('D') ];


/**
 * Prosigns
 */
class MorseCodeOEMProsign extends MorseCodeProsignToken { 
	constructor(){ super([]); }
	static GetVal(){ return MorseCodeProsignToken.GetVal( MorseCodeOEMProsign.Contents ) }
	/*Getting a particular mapping from the Dash Token*/
	static GetMap(){ return; }
}
MorseCodeOEMProsign.Contents = [ _('A'), _('R') ];

class MorseCodeSOSProsign extends MorseCodeProsignToken { 
	constructor(){ super([]); }
	static GetVal(){ return MorseCodeProsignToken.GetVal( MorseCodeSOSProsign.Contents ) }
	/*Getting a particular mapping from the Dash Token*/
	static GetMap(){ return; }
}
MorseCodeSOSProsign.Contents = [ _('S'), _('O'), _('S') ];



class MorseCodeMessage {
	constructor( sender = ``, recepient = ``, message = `` ) {
		this.sender    = sender;
		this.recepient = recepient;
		this.message   = message;
	}
	static InitMessage( sender = '', recepient = '' ) {
		return MorseCodeMessage.Transmit( sender + " DE " + recepient );
	}
	static InviteForMessage()  { return MorseCodeMessage.Transmit( "K" );  }
	static AcceptMessage( sender = '', recepient = '', wait = false ) {
		if( wait ) {
			// If the station called is unable to receive
			return MorseCodeMessage.Transmit( "AS" );
		} else {
			return MorseCodeMessage.Transmit( sender + " DE " + recepient + " K" );
		}
	}
	static EndMessage() { return MorseCodeMessage.Transmit( "SK" ); }
	static UnderstoodMessage() { return MorseCodeMessage.Transmit( "SN" ); }
	static EndTransmission() { return MorseCodeMessage.Transmit( `\+` ); }

	static Transmit( message ) {
		let code = [];
		let messageTokens = message.toUpperCase().split('').map( t => {
			code.push( MorseCodeToken.Get( t ) );
		} );
		let codeStr = MorseCodeWordToken.GetVal( code );
		MorseCodeWordToken.Output( code );
		return codeStr;
	}

	transmit( proper = true ) {
		let outStr = "";
		if( proper == true ) {
			outStr += MorseCodeMessage.InitMessage( this.sender, this.recepient );
			outStr += MorseCodeMessage.Transmit( "      " );
			outStr += MorseCodeMessage.AcceptMessage( this.sender, this.recepient, false );
			outStr += MorseCodeMessage.Transmit( "      " );
			outStr += MorseCodeMessage.Transmit( this.message );
			outStr += MorseCodeMessage.Transmit( " " );
			outStr += MorseCodeMessage.EndMessage();
			outStr += MorseCodeMessage.Transmit( " " );
			outStr += MorseCodeMessage.EndTransmission();
			outStr += MorseCodeMessage.Transmit( " " );
			outStr += MorseCodeMessage.InviteForMessage();
			outStr += MorseCodeMessage.Transmit( "      " );
			outStr += MorseCodeMessage.EndTransmission();
		} else {
			outStr = MorseCodeMessage.Transmit( this.message );
		}
		return outStr;
	}
}
