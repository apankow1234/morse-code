/** The Base Raw Tokens */
class MorseCodeBlankToken extends MorseCodeDelimiterToken { 
	constructor(){ super(); }
	/** Getting a particular mapping from the Blank Token */
	static GetMapping(){ return MorseCodeBlankToken.Mapping; }

	/** Getting the Value of the Blank Token */
	static GetVal(){ return; }
	static Output(){ return; }
}
MorseCodeBlankToken.Mapping = null;

class MorseCodeDotToken extends MorseCodeCodeToken { 
	constructor(){ super(); }
	/** Getting a particular mapping from the Dot Token */
	static GetMapping(){ return MorseCodeDotToken.Mapping; }

	/** Getting the Value of the Dot Token */
	static GetVal(){ return; }
	static Output(){ return; }
}
MorseCodeDotToken.Mapping  = null;

class MorseCodeDashToken extends MorseCodeCodeToken { 
	constructor(){ super(); }
	/** Getting a particular mapping from the Dash Token */
	static GetMapping(){ return MorseCodeDashToken.Mapping; }

	/** Getting the Value of the Dash Token */
	static GetVal(){ return; }
	static Output(){ return; }
}
MorseCodeDashToken.Mapping = null;





/** The Instanciable Raw Tokens - String Versions */
class MorseCodeBlankStringToken extends MorseCodeBlankToken { 
	constructor(){ super(); }
	/** Getting a particular mapping from the Blank Token */
	static GetMapping(){ return MorseCodeBlankToken.GetMapping(); }

	/** Getting the Value of the Blank Token */
	static GetVal(){ return MorseCodeBlankStringToken.Val; }
	static Output(){ return MorseCodeBlankStringToken.Val; }
}
MorseCodeBlankStringToken.Val   = ` `;

class MorseCodeDotStringToken extends MorseCodeDotToken { 
	constructor(){ super(); } 
	/** Getting a particular mapping from the Dot Token */
	static GetMapping(){ return MorseCodeDotToken.GetMapping(); }

	/** Getting the Value of the Dot Token */
	static GetVal(){ return MorseCodeDotStringToken.Val;   }
	static Output(){ return MorseCodeDotStringToken.Val;   }
}
MorseCodeDotStringToken.Val     = `.`;

class MorseCodeDashStringToken extends MorseCodeDashToken { 
	constructor(){ super(); } 
	/** Getting a particular mapping from the Dash Token */
	static GetMapping(){ return MorseCodeDashToken.GetMapping(); }

	/** Getting the Value of the Dash Token */
	static GetVal(){ return MorseCodeDashStringToken.Val;  }
	static Output(){ return MorseCodeDashStringToken.Val;  }
}
MorseCodeDashStringToken.Val     = `-`;





/** The Instanciable Raw Tokens - Signal Versions */
// const s = new Signal();
const s = new Tone();

class MorseCodeBlankSignalToken extends MorseCodeBlankToken { 
	constructor(){ super(); }
	/** Getting a particular mapping from the Blank Token */
	static GetMapping(){ return MorseCodeBlankToken.GetMapping(); }

	/** Getting the Value of the Blank Token */
	static GetVal(){ return MorseCodeBlankStringToken.GetVal(); }
	static Output(){ s.signal( 0, 1 );                          }
}

class MorseCodeDotSignalToken extends MorseCodeDotToken { 
	constructor(){ super(); } 
	/** Getting a particular mapping from the Dot Token */
	static GetMapping(){ return MorseCodeDotToken.GetMapping(); }

	/** Getting the Value of the Dot Token */
	static GetVal(){ return MorseCodeDotStringToken.GetVal();   }
	static Output(){ s.signal( 1, 1 ); s.signal( 0, 1 );        }
}

class MorseCodeDashSignalToken extends MorseCodeDashToken { 
	constructor(){ super(); } 
	/** Getting a particular mapping from the Dash Token */
	static GetMapping(){ return MorseCodeDashToken.GetMapping(); }

	/** Getting the Value of the Dash Token */
	static GetVal(){ return MorseCodeDashStringToken.GetVal();  }
	static Output(){ s.signal( 1, 3 ); s.signal( 0, 1 );        }
}
