class Signal {
	constructor(){
		this.unitLength = 0.05;
	}
	signal( gain = 1.0, units = 1 ) {}
	open(   gain = 1.0 ) {}
	close() {}
	start() {}
	stop()  {}
}
class Tone extends Signal {
	constructor() {
		super();
		this.context  = new (window.AudioContext || window.webkitAudioContext)();
		this.tone = this.context.createOscillator();
		this.gainCtrl = this.context.createGain();
		this.tone.connect( this.gainCtrl );
		this.tone.frequency.value = 400;
		this.tone.type = "sine";
		this.gainCtrl.connect( this.context.destination );
		this.t = this.context.currentTime;
		this.close();
		this.start();
	}
	signal( gain = 1.0, units = 1 ) {
		let duration = this.unitLength * units;
		this.t = ( this.t + duration < this.context.currentTime ) ? this.context.currentTime : this.t;
		this.gainCtrl.gain.setValueAtTime( gain, this.t );
		console.log( this.gainCtrl.gain );
		this.gainCtrl.gain.setValueAtTime( 0, this.t + duration );
		this.t += duration;
	}
	open( gain = 1.0 ) { this.gainCtrl.gain.setValueAtTime( gain, this.context.currentTime ); }
	close() { this.gainCtrl.gain.setValueAtTime( 0, this.context.currentTime ); }
	start() { this.tone.start( this.context.currentTime ); }
	stop()  { this.tone.stop(  this.context.currentTime ); }
}