class ParserToken { constructor(){} }
class MorseCodeToken extends ParserToken { 
	constructor(){ super(); }
	static GetByMappingVal( character ) {
		for( let i=0; i<MorseCodeToken.allTokens.length; i++ ) {
			let tkn = MorseCodeToken.allTokens[i];
			if( tkn.Mapping.Val == character ) {
				return tkn;
			}
		}
		return;
	} 
}
MorseCodeToken.allTokens = [];

class MorseCodeRawToken      extends MorseCodeToken { constructor(){ super(); } }
class MorseCodeCompoundToken extends MorseCodeToken {
	constructor( contents = [] ){ 
		super();
		this.Contents = contents;
	}
}

class MorseCodeDelimiterToken extends MorseCodeRawToken { constructor(){ super(); } }
class MorseCodeCodeToken      extends MorseCodeRawToken { constructor(){ super(); } }



/*Morse Code Compound Tokens*/
class MorseCodePatternToken extends MorseCodeCompoundToken { 
	constructor( pattern = [] ){ super( pattern ); }
	static GetVal( morseCodeRawTokenList ) {
		let value = "";
		for( let i=0; i < morseCodeRawTokenList.length; i++ ) {
			value += morseCodeRawTokenList[i].GetVal();
		}
		return value;
	}
	static Output( morseCodeRawTokenList ) {
		let value = "";
		for( let i=0; i < morseCodeRawTokenList.length; i++ ) {
			value += morseCodeRawTokenList[i].Output();
		}
		return value;
	}
}

class MorseCodeProsignToken extends MorseCodePatternToken {
	constructor(){ super([]); }
	static GetVal( prosign ) {
		let value = "";
		for( let i=0; i < prosign.Contents.length; i++ ) {
			if( prosign.Contents[i] instanceof MorseCodePatternToken ){
				for( let j=0; j < prosign.Contents[i].Contents.length; j++ ){
					value += prosign.Contents[i].Contents[j].GetVal();
				}
			} else {
				value += prosign.Contents[i].GetVal();
			}
		}
		return value;
	}
}
class MorseCodeWordToken extends MorseCodeCompoundToken { 
	constructor( contents = [] ){ super( contents ); } 
	static GetVal( patterns ) { 
		console.log( patterns );
		let value = "";
		for( let i=0; i < patterns.length; i++ ) {
			let abbr = ( patterns[i].__proto__.name == `MorseCodePatternToken` ) ? patterns[i].Contents : patterns[i];
			for( let j=0; j < abbr.length; j++ ){
				value += abbr[j].GetVal();
			}
			if( i < patterns.length - 1 ) {
				value += MorseCodeBlankSignalToken.GetVal() + 
				         MorseCodeBlankSignalToken.GetVal() + 
				         MorseCodeBlankSignalToken.GetVal();
			}
		}
		return value;
	}
	static Output( patterns ) { 
		console.log( patterns );
		let value = "";
		for( let i=0; i < patterns.length; i++ ) {
			let abbr = ( patterns[i].__proto__.name == `MorseCodePatternToken` ) ? patterns[i].Contents : patterns[i];
			for( let j=0; j < abbr.length; j++ ){
				value += abbr[j].Output();
			}
			if( i < patterns.length - 1 ) {
				value += MorseCodeBlankSignalToken.Output() + 
				         MorseCodeBlankSignalToken.Output() + 
				         MorseCodeBlankSignalToken.Output();
			}
		}
		return value;
	}
}


class MorseCodeAbbreviationToken extends MorseCodeWordToken { 
	constructor( contents = [] ){ super( contents ); } 
	static GetVal( patterns ) { return MorseCodeWordToken.GetVal( patterns.Contents ); }
}



class HumanReadableToken extends ParserToken { constructor(){ super(); } }
HumanReadableToken.allTokens = [];
class HumanReadableRawToken extends HumanReadableToken { constructor(){ super(); } }

class HumanReadableDelimiterToken extends HumanReadableRawToken { constructor(){ super(); } }
class HumanReadableGlyphToken extends HumanReadableRawToken { constructor(){ super(); } }


class HumanReadableCompoundToken extends HumanReadableToken { constructor(){ super(); } }

