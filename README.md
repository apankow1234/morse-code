# Morse Code Parser

![Icon](images/icon-sm.png) 

[Live Link](https://apankow1234.gitlab.io/morse-code/test.html) 



## International Morse Code ITU-R M.1677-1

```
di & dit             dot             .

dah                  dash            -

hmm                  blank           ' '



Letter               A               [ di,  dah ]

Letter               B               [ dah, di,  di,  dit ]

Letter               C               [ dah, di,  dah, dit ]

Letter               D               [ dah, di,  dit ]

Letter               E               [ dit ]

Letter               F               [ di,  di,  dah, dit ]

Letter               G               [ dah, dah, dit ]

Letter               H               [ di,  di,  di,  dit ]

Letter               I               [ di,  dit ]

Letter               J               [ di,  dah, dah, dah ]

Letter               K               [ dah, di,  dah ]

Letter               L               [ di,  dah, di,  dit ]

Letter               M               [ dah, dah ]

Letter               N               [ dah, dit ]

Letter               O               [ dah, dah, dah ]

Letter               P               [ di,  dah, dah, dit ]

Letter               Q               [ dah, dah, di,  dah ]

Letter               R               [ di,  dah, dit ]

Letter               S               [ di,  di,  dit ]

Letter               T               [ dah ]

Letter               U               [ di,  di,  dah ]

Letter               V               [ di,  di,  di,  dah ]

Letter               W               [ di,  dah, dah ]

Letter               X               [ dah, di,  di,  dah ]

Letter               Y               [ dah, di,  dah, dah ]

Letter               Z               [ dah, dah, di,  dit ]

Digit                0               [ dah, dah, dah, dah, dah ]

Digit                1               [ di,  dah, dah, dah, dah ]

Digit                2               [ di,  di,  dah, dah, dah ]

Digit                3               [ di,  di,  di,  dah, dah ]

Digit                4               [ di,  di,  di,  di,  dah ]

Digit                5               [ di,  di,  di,  di,  dit ]

Digit                6               [ dah, di,  di,  di,  dit ]

Digit                7               [ dah, dah, di,  di,  dit ]

Digit                8               [ dah, dah, dah, di,  dit ]

Digit                9               [ dah, dah, dah, dah, dit ]

Pattern Separator    ''              [ hmm, hmm, hmm ]

Word Separator       ' '             [ hmm, hmm, hmm, hmm, hmm, hmm, hmm ]

Full Stop            .               [ di,  dah, di,  dah, di,  dah ]

Comma                ,               [ dah, dah, di,  di,  dah, dah ]

Colon                :               [ dah, dah, dah, di,  di,  dit ]

Question Mark        ?               [ di,  di,  dah, dah, di,  dit ]

Apostrophe           '               [ di,  dah, dah, dah, dah, dit ]

Hyphen               -               [ di,  dah, dah, dah, dah, dit ]

Division Sign        /               [ dah, di,  di,  dah, dit ]

Parenthesis Left     (               [ dah, di,  dah, dah, dit ]

Parenthesis Right    )               [ dah, di,  dah, dah, di,  dah ]

Quotation Mark       "               [ di,  dah, di,  di,  dah, dit ]

Equals Sign          =               [ dah, di,  di,  di,  dah ]

Plus Sign            +               [ di,  dah, di,  dah, dit ]

Multiplication Sign  ×               [ dah, di,  di,  dah ]

Commercial At        @               [ di,  dah, dah, di,  dah, dit ]
```